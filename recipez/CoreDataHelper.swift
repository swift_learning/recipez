//
//  CoreDataHelper.swift
//  recipez
//
//  Created by iulian david on 11/13/16.
//  Copyright © 2016 iulian david. All rights reserved.
//

import UIKit
import CoreData


//Singleton for CRUD core data operations
class CoreDataHelper{
    
    //The singleton instance
    static let instance = CoreDataHelper()
    
    /**
        Grabing a managed object context of type **NSManagedObjectContext**
        Pull up the application delegate and grab a reference to its persistent container to get your hands on its NSManagedObjectContext
    */
    func getPersistenceContext() -> NSManagedObjectContext? {
        
        var context: NSManagedObjectContext?
        
        var appDelegate: AppDelegate?
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        context = appDelegate?.persistentContainer.viewContext
        
        return context
    }
    
    /**
        Adds a new Recipe in the Core Data
        Steps to be done:
        1. Get a managed object context see ```getPersistenceContext```
        2. Create a new managed object and insert it into the managed object context. You can do this in one step with NSManagedObject’s static method: ```entity(forEntityName:in:)```
        An entity description is the piece linking the entity definition from your Data Model with an instance of NSManagedObject at runtime.
        3. With an NSManagedObject in hand, you set the name attribute using key-value coding. 
        You must spell the KVC key (name in this case) exactly as it appears in your Data Model, otherwise your app will crash at runtime.
        4. You commit your changes to person and save to disk by calling save on the managed object context. 
        Note save can throw an error, which is why you call it using the try keyword within a do-catch block. 
        Finally, insert the new managed object into the people array so it shows up when the table view reloads.
     
        - Parameter title: The title of the Recipe
        - Parameter img: The image for the Recipe, is of UIImage type
        - Parameter ingredients: The ingredients of the Recipe
        - Parameter steps: The steps to take for creating the recipe
     
    */
    func addRecipe(title: String, img: UIImage, ingredients: String, steps: String){
        
        //1
        guard let context = getPersistenceContext()! as NSManagedObjectContext?  else {
            return
        }
        
        // 2
        let entity =
            NSEntityDescription.entity(forEntityName: kEntityName,
                                       in: context)!
        let recipe = Recipe(entity: entity,
                            insertInto: context)
        // 3
        // The standard way would be: recipe.setValue(title, forKeyPath: "title")
        recipe.title = title
        recipe.setRecipeImage(img: img)
        recipe.ingredients = ingredients
        recipe.steps = steps
        
        
        //4
        do{
            try context.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    /**
     Return an array of Recipe from the Core Data through the NSManagedObjectContext
     
     Steps to be done:
     1. Get a managed object context see ```getPersistenceContext```
     2. NSFetchRequest is the class responsible for fetching from Core Data. Fetch requests are both powerful and flexible.
     Fetch requests have several qualifiers used to refine the set of results returned.
     Setting a fetch request’s entity property, or alternatively initializing it with init(entityName:), fetches all objects of a particular entity.
     Also note NSFetchRequest is a generic type. This use of generics specifies a fetch request’s expected return type, in this case Recipe.
     3. Hand the fetch request over to the managed object context to do the heavy lifting.
     You **fetch(_:)** returns an array of managed objects meeting the criteria specified by the fetch request.
     
     - Returns: An array of Recipe the recipes stored in Core Data
     
    */
    func getRecipes() -> [Recipe] {
        
        var recipes = [Recipe]()
        
        //1
        guard let context = getPersistenceContext()! as NSManagedObjectContext?  else {
            return recipes
        }
        //2
        let fetchRequest = NSFetchRequest<Recipe>(entityName: kEntityName)
        
        //3
        //
        do {
            recipes = try context.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return recipes
    }
    
    /** 
     Deletes all the Recipe instances from Core Data
     */
    func deleteAllRecipes() -> Bool{
        //1
        guard let context = getPersistenceContext()! as NSManagedObjectContext?  else {
            return false
        }
        
        let fetchRequest = NSFetchRequest<Recipe>(entityName: kEntityName)
        
        if let result = try? context.fetch(fetchRequest) {
            for object in result {
                context.delete(object)
            }
        }
        
        do{
            try context.save()
        } catch let error as NSError {
            print("Could not delete. \(error), \(error.userInfo)")
            return false
        }
        return true
    }
    
    /**
     Deletes a Recipe
     
     - Parameter recipe: The Recipe instance to be deleted
     - Returns wether the operation executed or not
    */
    func deleteRecipe(recipe: Recipe) -> Bool {
        
        //1
        guard let context = getPersistenceContext()! as NSManagedObjectContext?  else {
            return false
        }
        
        context.delete(recipe)
        
        do {
            try context.save()
        } catch let error as NSError {
            print("Error While Deleting Recipe: \(error.userInfo)")
            return false
        }
        
        return true
    }
    
    /** 
     Returns a Recipe instance from the Core Data
     */
    func getRecipe(at: Int) -> Recipe?{
        return getRecipes()[at]
    }
    
    /**
     Updates the given Recipe with the parameters passed
     - Parameter recipe: The Recipe we want to update
     - Parameter title: The title of the Recipe
     - Parameter img: The image the Recipe
     - Parameter ingredients: The ingredients used for the Recipe
     - Parameter steps: The steps of the Recipe
     */
    func updateRecipe(_ recipe: Recipe, title: String, img: UIImage, ingredients: String, steps: String) {
        recipe.setRecipeImage(img: img)
        recipe.title = title
        recipe.ingredients = ingredients
        recipe.steps = steps
        do {
            try recipe.managedObjectContext?.save()
        } catch {
            print("Error occured during updating entity")
        }
    }
}
