//
//  Recipe+CoreDataProperties.swift
//  recipez
//
//  Created by iulian david on 11/13/16.
//  Copyright © 2016 iulian david. All rights reserved.
//

import Foundation
import CoreData


extension Recipe {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Recipe> {
        return NSFetchRequest<Recipe>(entityName: kEntityName);
    }

    @NSManaged public var image: NSData?
    @NSManaged public var ingredients: String?
    @NSManaged public var steps: String?
    @NSManaged public var title: String?

}
