//
//  ViewController.swift
//  recipez
//
//  Created by iulian david on 11/13/16.
//  Copyright © 2016 iulian david. All rights reserved.
//

import UIKit
import CoreData

/**
 Main controller with a UITableView containing the Recipes added
 
 ### USES SWIPE FOR MODIFYING DATA
 ```
 let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swiped(_:)))
 swipeRight.direction = UISwipeGestureRecognizerDirection.right
 tableView.addGestureRecognizer(swipeRight)
 ```
 - SWIPE LEFT FOR MODIFY
 - SWIPE RIGHT DOR DELETION
 
 To use it with tap one must register a UITapGestureRecognizer and the number of taps
 ```
 let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
 tap.numberOfTouchesRequired = 2
 tableView.addGestureRecognizer(tap)
 ```
 
 Can be configured to clear the Core data, by adding in ```viewDidLoad``
 ``` 
 CoreDataHelper.instance.deleteAllRecipes() 
 ```
 */
class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var recipes = [Recipe]()
  
    let segueAddRecipe = "AddRecipe"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swiped(_:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        tableView.addGestureRecognizer(swipeRight)

        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swiped(_:)))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        tableView.addGestureRecognizer(swipeLeft)
    
    }


    override func viewDidAppear(_ animated: Bool) {
        fetchAndSetResults()
        tableView.reloadData()
    }
    
    
    func fetchAndSetResults(){
        
        recipes = CoreDataHelper.instance.getRecipes()
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeCell") as? RecipeCell{
            let recipe = recipes[indexPath.row]
            cell.configureCell(recipe: recipe)
            return cell
        } else {
            return RecipeCell()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipes.count
    }

    
    @IBAction func newButtonPressed(_ sender: Any) {
         self.performSegue(withIdentifier: segueAddRecipe, sender: -1)
        
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueAddRecipe {
            let vc : RecipeVC = segue.destination as! RecipeVC
            vc.editRecNo = sender as! Int
            if(vc.editRecNo != -1) {
                vc.recipe = recipes[vc.editRecNo]
            }
        }
    }
    
    
    func swiped(_ gesture: UIGestureRecognizer){
        if let swipeGesture = gesture as? UISwipeGestureRecognizer{
            let swipeLocation = gesture.location(in: self.tableView)
            if let swipedIndexPath = tableView.indexPathForRow(at: swipeLocation) {
                
                if (self.tableView.cellForRow(at: swipedIndexPath) as? RecipeCell) != nil {
                    switch swipeGesture.direction {
                    case UISwipeGestureRecognizerDirection.right:
                        if (CoreDataHelper.instance.deleteRecipe(recipe: recipes[swipedIndexPath.row])) {
                            recipes.remove(at: swipedIndexPath.row)
                            tableView.reloadData()
                        }
                    case UISwipeGestureRecognizerDirection.left:
                        self.performSegue(withIdentifier: segueAddRecipe, sender: swipedIndexPath.row)
                    default:
                        print("other swipe")
                    }
                }
            }
        }
    }
}

