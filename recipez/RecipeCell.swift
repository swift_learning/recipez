//
//  RecipeCell.swift
//  recipez
//
//  Created by iulian david on 11/13/16.
//  Copyright © 2016 iulian david. All rights reserved.
//

import UIKit

class RecipeCell: UITableViewCell {

    @IBOutlet weak var recipeImg: UIImageView?
    @IBOutlet weak var recipeTitle: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func configureCell(recipe: Recipe) {
        recipeTitle?.text = recipe.title
        recipeImg?.image = recipe.getRecipeImg()
    }

   
}
