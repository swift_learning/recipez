//
//  RecipeVC.swift
//  recipez
//
//  Created by iulian david on 11/13/16.
//  Copyright © 2016 iulian david. All rights reserved.
//

import UIKit
import CoreData
class RecipeVC: UIViewController, UIImagePickerControllerDelegate
    , UINavigationControllerDelegate {

    
    @IBOutlet weak var recipeTitle: UITextField!
    @IBOutlet weak var recipeIngredients: UITextField!
    @IBOutlet weak var recipeSteps: UITextField!
    @IBOutlet weak var recipeImage: UIImageView!
    
    var imagePicker : UIImagePickerController!
    
    //record to edit if needed
    var editRecNo = Int()
    
    var recipe: Recipe?
    
    var recipeArray: [Recipe]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        recipeImage.layer.cornerRadius = 5.0
        recipeImage.clipsToBounds = true
        
        //means we're modifying
        if editRecNo != -1 {
//            if let recipe = recipeArray?[editRecNo] {
            if(recipe != nil){
              self.recipeImage.image = recipe?.getRecipeImg()
              self.recipeTitle.text = recipe?.title
                self.recipeIngredients.text = recipe?.ingredients
                self.recipeSteps.text = recipe?.steps
            }
        }
    }

    /**
     Shows an image chooser ,
     pay attention to app settings 
     ![Photo Access](Photo Library Access.png)
    */
    @IBAction func addPicture(_ sender: UIButton) {
        sender.setTitle("", for: .normal)
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func addRecipe(_ sender: Any) {
        if let title = recipeTitle.text
        {
            if editRecNo != -1 {
                CoreDataHelper.instance.updateRecipe(recipe!, title: title, img: recipeImage.image!, ingredients: recipeIngredients.text!, steps: recipeSteps.text!)
            } else {
                CoreDataHelper.instance.addRecipe(title: title, img: recipeImage.image!, ingredients: recipeIngredients.text!, steps: recipeSteps.text!)
            }
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage  else {
            print("Something went wrong")
            return
        }
        recipeImage.image = image
        imagePicker.dismiss(animated: true, completion: nil)
    }

}
