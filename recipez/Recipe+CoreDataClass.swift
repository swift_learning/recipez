//
//  Recipe+CoreDataClass.swift
//  recipez
//
//  Created by iulian david on 11/13/16.
//  Copyright © 2016 iulian david. All rights reserved.
//

import Foundation
import CoreData
import UIKit

@objc(Recipe)
public class Recipe: NSManagedObject {
    
    func setRecipeImage(img: UIImage){
        let data = UIImagePNGRepresentation(img)
        self.image = data as NSData?
    }
    
    func getRecipeImg() -> UIImage {
        let img = UIImage(data: self.image! as Data)! //force unrraping with !
        return img
    }
}
