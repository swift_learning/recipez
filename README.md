# README #

Explainin priciples of Core Data

### Implementing A Recipe App with the use of Core Data ###

* Swift3 with Core Data
* [Core Data Implementation](https://www.raywenderlich.com/145809/getting-started-core-data-tutorial)

### How do I get set up? ###

* Create a a new project with Core Data enabled
* For Photo Library Access(ios 10 will crash if not set) ![Photo Library Access.png](https://bitbucket.org/repo/x49Lpk/images/3484449711-Photo%20Library%20Access.png)
* Database configuration: For Xcode 8  ![coreData Xcode8 Configuration.png](https://bitbucket.org/repo/x49Lpk/images/1038252460-coreData%20Xcode8%20Configuration.png)


### TO DO ###

* Writing tests
* Code review
* Option for deleting the recipe
* Modify recipe


### Screens ###

1. Initial Screen 
![Simulator Screen Shot Nov 13, 2016, 12.42.49 PM.jpg](https://bitbucket.org/repo/x49Lpk/images/1941724132-Simulator%20Screen%20Shot%20Nov%2013,%202016,%2012.42.49%20PM.jpg)
2. Add Recipe
![Simulator Screen Shot Nov 13, 2016, 12.42.35 PM.jpg](https://bitbucket.org/repo/x49Lpk/images/1791000988-Simulator%20Screen%20Shot%20Nov%2013,%202016,%2012.42.35%20PM.jpg)
